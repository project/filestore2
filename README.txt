
Filestore2
----------

Filestore2 requires fscache to store all files.

Problems.
---------

I found a problem where fscache has been deleting files that have been
updated on an existing node. thus the file cannot be downloaded. Use the
following SQL to determine if you have the problem and which files need to
be re-uploaded.

SELECT fs.nid, fs.fsid FROM filestore2 fs 
LEFT JOIN fscache fsc ON fs.fsid = fsc.fsid WHERE fsc.fsid IS NULL

Theme Tips
----------

To style the INPUT.FILE form element to look more like the rest of your site
use the following trick

http://www.quirksmode.org/dom/inputfile.html
